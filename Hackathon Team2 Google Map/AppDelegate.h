//
//  AppDelegate.h
//  Hackathon Team2 Google Map
//
//  Created by click labs 115 on 11/27/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

