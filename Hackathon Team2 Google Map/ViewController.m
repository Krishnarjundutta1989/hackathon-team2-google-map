//
//  ViewController.m
//  Hackathon Team2 Google Map
//
//  Created by click labs 115 on 11/27/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"
@interface ViewController (){
    CLLocationManager *manager;
    CLLocation *currentLocation;
    NSString *city;
    NSString *state;
    CLLocationCoordinate2D position;
    GMSMarker *marker;
    NSString *Country;
    NSString *URL;
    NSString *address;
    NSMutableArray *arrayForFormattedAddress;
    UITableViewCell *cell;
    CLLocation *newLocation2;
    CLLocation *newLocation1;
    NSString *lat;
    NSString *lng;
    UIAlertView *alert;
}
@property (strong, nonatomic) IBOutlet UITableView *tblForFormattedAddress;
@property (strong, nonatomic) IBOutlet UISearchBar *searchCity;
@property (strong, nonatomic) IBOutlet UIButton *btnCurrentLocation;
@property (strong, nonatomic) IBOutlet UIButton *btnRoute;
@property (nonatomic, strong) NSMutableArray *markersArray;

@end

@implementation ViewController
@synthesize tblForFormattedAddress;
@synthesize searchCity;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _markersArray = [[NSMutableArray alloc]init];
    
    arrayForFormattedAddress = [NSMutableArray new];
    manager = [[CLLocationManager alloc]init];
    manager.delegate = self;
    manager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [manager startUpdatingLocation];
    
    tblForFormattedAddress.hidden = YES;
    self.googleMap.delegate = self;
    searchCity.delegate = self;
    
    [manager requestWhenInUseAuthorization];
    [self.view addSubview:searchCity];
    [self.view addSubview:tblForFormattedAddress];
    [self.view addSubview:_btnRoute];
    [self.view addSubview:_btnCurrentLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Current Location Please Enter Location Manually" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    NSLog(@"didUpdateToLocation: %@", newLocation);
    currentLocation = newLocation;
    
    if (currentLocation != nil) {
        
        position = currentLocation.coordinate;
        [self getaddress];
    }
    
}
-(void) getaddress  {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    newLocation1 = [[CLLocation alloc]initWithLatitude:currentLocation.coordinate.latitude
                                             longitude:currentLocation.coordinate.longitude];
    
    [geocoder reverseGeocodeLocation:newLocation1
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       
                       if (error) {
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                       }
                       
                       if (placemarks && placemarks.count > 0)
                       {
                           CLPlacemark *placemark = placemarks[0];
                           
                           NSDictionary *addressDictionary =
                           placemark.addressDictionary;
                           
                           NSLog(@"%@ ", addressDictionary);
                           city = addressDictionary[@"City"];
                           NSLog(@"%@",city);
                           marker = [GMSMarker markerWithPosition:position];
                           marker.title = [NSString stringWithFormat:@"%@",city];
                           
                           marker.map = _googleMap;

                           
                       }
                   }];
    [manager startUpdatingLocation];
    
}
- (void)mapView:(GMSMapView *)mapView
didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    [manager stopUpdatingLocation];
    position = coordinate;
    marker = [GMSMarker markerWithPosition:position];
    [self CilkAndMark];
    
}

-(void) CilkAndMark  {
    CLGeocoder *geocoder2 = [[CLGeocoder alloc] init];
    
    newLocation2 = [[CLLocation alloc]initWithLatitude:position.latitude
                                             longitude:position.longitude];
    NSString * Lati = [NSString stringWithFormat:@"%f", position.latitude];
    NSString * Longi = [NSString stringWithFormat:@"%f",position.longitude];
    [_markersArray addObject: Lati];
    [_markersArray addObject: Longi];
    [geocoder2 reverseGeocodeLocation:newLocation2
                    completionHandler:^(NSArray *placemarks, NSError *error) {
                        
                        if (error) {
                            NSLog(@"Geocode failed with error: %@", error);
                            return;
                        }
                        
                        if (placemarks && placemarks.count > 0)
                        {
                            CLPlacemark *placemark = placemarks[0];
                            
                            NSDictionary *addressDictionary =
                            placemark.addressDictionary;
                            
                            NSLog(@"%@ ", addressDictionary);
                            if (addressDictionary[@"City"] != NULL ){
                                city = addressDictionary[@"City"];
                                NSLog(@"%@",city);
                                marker = [GMSMarker markerWithPosition:position];
                                marker.title = [NSString stringWithFormat:@"%@",city];
                                
                                marker.map = _googleMap;
                                
                            }
                            else if ((addressDictionary[@"State"] != NULL )){
                                state = addressDictionary[@"State"];
                                NSLog(@"%@",state);
                                marker = [GMSMarker markerWithPosition:position];
                                marker.title = [NSString stringWithFormat:@"%@",state];
                                
                                marker.map = _googleMap;
                                
                            }
                            
                            else if ((addressDictionary[@"Country"] != NULL )){
                                Country = addressDictionary[@"Country"];
                                NSLog(@"%@",Country);
                                marker = [GMSMarker markerWithPosition:position];
                                marker.title = [NSString stringWithFormat:@"%@",Country];
                                
                                marker.map = _googleMap;
                                
                            }
                        }
                        
                    }];
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    address = searchText;
    NSString *encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                                    NULL,
                                                                                                    (CFStringRef)address,
                                                                                                    NULL,
                                                                                                    (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                    kCFStringEncodingUTF8 ));
    URL = [NSString
           stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?address=%@&key=AIzaSyBhCly2RqKlh8c0-UmrpIPzTwsXpu33ojo",encodedString];
    NSData* data = [NSData dataWithContentsOfURL:[NSURL URLWithString:URL]];
    
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    NSArray *searchAddress = json[@"results"];
    [arrayForFormattedAddress removeAllObjects];
    for (int i=0; i<searchAddress.count; i++) {
        
        NSDictionary *searchFound = searchAddress[i];
        if ((searchFound[@"geometry"] != NULL)&&(searchFound[@"formatted_address"] != NULL)) {
            NSDictionary *Geometry = searchFound[@"geometry"];
            NSDictionary *location = Geometry[@"location"];
            lat = location[@"lat"];
            lng = location[@"lng"];
            
            address = searchFound[@"formatted_address"];
            position = CLLocationCoordinate2DMake(lat.doubleValue,lng.doubleValue);
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:position.latitude
                                                                    longitude:position.longitude
                                                                         zoom:5.0];
            [_googleMap animateToCameraPosition:camera];
            marker = [GMSMarker markerWithPosition:position];
            marker.title = [NSString stringWithFormat:@"%@",address];
            marker.map = _googleMap;
            [arrayForFormattedAddress addObject:address];
            
        }
        
    }
    [tblForFormattedAddress reloadData];
    
}
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
    
}
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrayForFormattedAddress.count;
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    cell = [tableView dequeueReusableCellWithIdentifier:@"reuse"];
    cell.textLabel.text = arrayForFormattedAddress [indexPath.row];
    tblForFormattedAddress.hidden = NO;
    
    return cell;
}
-(void)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Space Not Allow Only Comma Allow" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    cell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *formattedAdress = [NSString stringWithFormat:@"%@",cell.textLabel.text];
    NSString *encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                                    NULL,
                                                                                                    (CFStringRef)formattedAdress,
                                                                                                    NULL,
                                                                                                    (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                    kCFStringEncodingUTF8 ));
    URL = [NSString
           stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?address=%@&key=AIzaSyBhCly2RqKlh8c0-UmrpIPzTwsXpu33ojo",encodedString];
    NSData* data = [NSData dataWithContentsOfURL:[NSURL URLWithString:URL]];
    
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    NSArray *searchAddress = json[@"results"];
    for (int i=0; i<searchAddress.count; i++) {
        NSDictionary *searchFound = searchAddress[i];
        if ((searchFound[@"geometry"] != NULL)&&(searchFound[@"formatted_address"] != NULL)) {
            NSDictionary *Geometry = searchFound[@"geometry"];
            NSDictionary *location = Geometry[@"location"];
            lat = location[@"lat"];
            lng = location[@"lng"];
            [self getWayPointsFromMarker];
            
            address = searchFound[@"formatted_address"];
            position = CLLocationCoordinate2DMake(lat.doubleValue,lng.doubleValue);
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:position.latitude
                                                                    longitude:position.longitude
                                                                         zoom:7.0];
            [_googleMap animateToCameraPosition:camera];
            marker = [GMSMarker markerWithPosition:position];
            marker.title = [NSString stringWithFormat:@"%@",address];
            marker.map = _googleMap;
            
            break;
        }
        
    }
    tblForFormattedAddress.hidden = TRUE;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row %2) {
        [cell setBackgroundColor:[UIColor grayColor]] ;
    }
    else{
        [cell setBackgroundColor:[UIColor lightGrayColor]];
    }
}

-(void)getWayPointsFromMarker{
    newLocation1 = [[CLLocation alloc]initWithLatitude:currentLocation.coordinate.latitude
                                             longitude:currentLocation.coordinate.longitude];
    NSString * str = [NSString stringWithFormat:@"%@",newLocation1];
    NSArray *destinationLocation = [NSArray alloc];
    destinationLocation = [str componentsSeparatedByString:@"+"];
    
    NSString * oLat = [destinationLocation objectAtIndex:1];
    NSString *currentLat = [oLat
                            stringByReplacingOccurrencesOfString:@"," withString:@""];
    NSString * oLag = [destinationLocation objectAtIndex:2];
    NSString *currentLng = [oLag
                            stringByReplacingOccurrencesOfString:@"> " withString:@""];
    
    NSString *url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?&origin=%@,%@&destination=%@,%@&sensor=false",currentLat,currentLng,lat ,lng];
    NSLog(@"url : %@", url);
    
    NSURL *googleRequestURL=[NSURL URLWithString:url];
    NSLog(@"googleRequestURL : %@", googleRequestURL);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSData* data = [NSData dataWithContentsOfURL: googleRequestURL];
        [self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone:YES];
    });
    
}
- (IBAction)btnRouteClick:(id)sender {
    NSString * currentLat = [_markersArray objectAtIndex:0];
    NSString * currentLng = [_markersArray objectAtIndex:1];
    NSString * destinationLat = [_markersArray objectAtIndex:2];
    NSString * destinationLng = [_markersArray objectAtIndex:3];
    NSString *url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?&origin=%@,%@&destination=%@,%@&sensor=false",currentLat,currentLng,destinationLat,destinationLng];
    NSLog(@"url : %@", url);
    
    NSURL *googleRequestURL=[NSURL URLWithString:url];
    NSLog(@"googleRequestURL : %@", googleRequestURL);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSData* data = [NSData dataWithContentsOfURL: googleRequestURL];
        [self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone:YES];
    });
}
- (void)fetchedData:(NSData *)responseData {
    //parse out the json data
    NSError* error;
    //  NSLog(@"responseData Data: %@", responseData);
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData
                          
                          options:kNilOptions
                          error:&error];
    NSArray* places = [json objectForKey:@"routes"];
    
    NSDictionary *routes = [json objectForKey:@"routes"][0];
    
    NSDictionary *route = [routes objectForKey:@"overview_polyline"];
    
    NSArray *routes1 = [json objectForKey:@"routes"];
    NSArray *legs = [routes1[0] objectForKey:@"legs"];
    NSLog(@"legs %@", legs);
    NSArray *steps = [legs[0] objectForKey:@"steps"];
    
    NSString *overview_route = [route objectForKey:@"points"];
    GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
    GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
    polyline.strokeColor = [UIColor redColor];
    polyline.strokeWidth = 5.f;
    polyline.map = _googleMap;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
