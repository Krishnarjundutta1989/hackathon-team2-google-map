//
//  ViewController.h
//  Hackathon Team2 Google Map
//
//  Created by click labs 115 on 11/27/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface ViewController : UIViewController<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,GMSMapViewDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet GMSMapView *googleMap;


@end

